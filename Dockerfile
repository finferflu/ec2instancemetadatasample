FROM golang:1.16

ADD bin/ec2meta /usr/bin/ec2meta

ENTRYPOINT [ "/usr/bin/ec2meta" ]