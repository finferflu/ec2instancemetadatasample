package getec2meta

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetMeta(t *testing.T) {
	getResponse = func(path string) (s string, err error) {
		switch path {
		case "/":
			s = `ami-id
iam/
test/
`
		case "/ami-id":
			s = "ami-test"
		case "/test/":
			s = "extra/"
		case "/test/extra/":
			s = "nested"
		case "/test/extra/nested":
			s = "nested_content"
		case "/iam/":
			s = "info"
		case "/iam/info":
			s = `{
	"Code" : "Success",
	"LastUpdated" : "2021-06-20T16:34:43Z",
	"InstanceProfileArn" : "arn:aws:iam::123:instance-profile/db-access-instance-profile-abc",
	"InstanceProfileId" : "ABC"
}`
		default:
			return "", errors.New(path + " not found")
		}
		return
	}

	expected := `{"ami-id":"ami-test","iam":{"info":"{\"Code\":\"Success\",\"InstanceProfileArn\":\"arn:aws:iam::123:instance-profile/db-access-instance-profile-abc\",\"InstanceProfileId\":\"ABC\",\"LastUpdated\":\"2021-06-20T16:34:43Z\"}"},"test":{"extra":{"nested":"nested_content"}}}`
	j, err := GetInstanceMetadata()
	assert.Nil(t, err)
	assert.Equal(t, expected, j)
}
