package getec2meta

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

// EC2Meta holds the EC2 instance metadata
type EC2Meta interface{}

var (
	addr = "http://169.254.169.254/latest/meta-data"

	getResponse = func(path string) (s string, err error) {
		resp, err := http.Get(addr)
		if err != nil {
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return
		}

		s = string(body)
		return
	}
)

// GetInstanceMetadata returns the EC2 instance metadata in JSON format
func GetInstanceMetadata() (s string, err error) {
	m, err := crawl("/")
	if err != nil {
		return
	}

	b, err := json.Marshal(m)
	if err != nil {
		return
	}

	return string(b), nil
}

func parseResponse(url string) (s []string, err error) {
	resp, err := getResponse(url)
	if err != nil {
		return
	}

	// Return escaped string if the returned object is in JSON format
	r, j := isJSON(resp)
	if r {
		b, err := json.Marshal(j)
		if err != nil {
			return s, err
		}
		return []string{string(b)}, nil
	}

	for _, str := range strings.Split(resp, "\n") {
		if str != "" {
			s = append(s, str)
		}
	}

	return
}

func isJSON(s string) (r bool, j interface{}) {
	if err := json.Unmarshal([]byte(s), &j); err != nil {
		return false, j
	}
	return true, j
}

func crawl(url string) (m EC2Meta, err error) {
	meta := make(map[string]interface{})
	resp, err := parseResponse(url)
	if err != nil {
		return
	}

	for _, item := range resp {
		// Keep crawling if item has children
		if strings.HasSuffix(item, "/") {
			val, err := crawl(url + item)
			if err != nil {
				return val, err
			}
			// Assign returned EC2Meta object to the current item
			// (with the trailing / stripped out)
			meta[item[:len(item)-1]] = val
		} else {
			// Get item value if this is the final child
			val, err := parseResponse(url + item)
			if err != nil {
				return val, err
			}
			// Expect the returned slice to only have 1 item
			meta[item] = val[0]
		}
	}

	return meta, nil
}
