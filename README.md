# ec2InstanceMetaSample

A sample script to pull EC2 instance metadata and print it in JSON format. Please note that this has not been tested against real data at this stage.

To pull specific keys from the metadata object, you can use the [objectQuerySample](https://gitlab.com/finferflu/objectquerysample) container, e.g.:

```sh
docker run -i --rm registry.gitlab.com/finferflu/ec2instancemetadatasample | docker run -i --rm registry.gitlab.com/finferflu/objectquerysample -k iam/info
```