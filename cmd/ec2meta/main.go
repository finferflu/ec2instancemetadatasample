package main

import (
	"ec2meta/internal/getec2meta"
	"fmt"
	"os"
)

func main() {
	s, err := getec2meta.GetInstanceMetadata()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(s)
}
